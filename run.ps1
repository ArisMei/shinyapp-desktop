$shiny = Start-Job -Name "shiny" -ArgumentList($pwd) -ScriptBlock{
    param($workingdir);
    cd $workingdir;
    & "rscript.exe" -e "shiny::runApp('.')" 2>&1 | % {
	    if($_ -like '*Listening on*'){
            ("$_" -replace ".*Listening on ",'').Trim()
        }
    }
} 

while ($shiny.HasMoreData -or $shiny.State -eq "Running") {     
    $url = $shiny.ChildJobs[0].output.readall()
    if($url){
        break;
    }
}
$args = @('-profile','./profile','-new-instance',"-url `"$url`"");
$ff = Start-Process "C:\Program Files\Mozilla Firefox\firefox.exe" -ArgumentList $args -PassThru -Wait
Stop-Job $shiny.Id
